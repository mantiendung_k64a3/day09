<?php 
$list_Q = array(
    [
        'question' => 'Do you love me?',
        'answer' => [
            "A" => "Yes",
            "B" => "A",
            "C" => "B",
            "D" => "C"
        ]
    ],
    [
        'question' => 'Do you love me??',
        'answer' => [
            "A" => "Yes",
            "B" => "A",
            "C" => "B",
            "D" => "C"
        ]
    ],
    [
        'question' => 'Do you love me???',
        'answer' => [
            "A" => "Yes",
            "B" => "A",
            "C" => "B",
            "D" => "C"
        ]
    ],
    [
        'question' => 'Do you love me????',
        'answer' => [
            "A" => "Yes",
            "B" => "A",
            "C" => "B",
            "D" => "C"
        ]
    ],
    [
        'question' => 'Do you love me?????',
        'answer' => [
            "A" => "Yes",
            "B" => "A",
            "C" => "B",
            "D" => "C"
        ]
    ],
    [
        'question' => 'Do you love me??????',
        'answer' => [
            "A" => "Yes",
            "B" => "A",
            "C" => "B",
            "D" => "C"
        ]
    ],
    [
        'question' => 'Do you love me????????',
        'answer' => [
            "A" => "Yes",
            "B" => "A",
            "C" => "B",
            "D" => "C"
        ]
    ],
    [
        'question' => 'Do you love me?????????',
        'answer' => [
            "A" => "Yes",
            "B" => "A",
            "C" => "B",
            "D" => "C"
        ]
    ],
    [
        'question' => 'Do you love me??????????',
        'answer' => [
            "A" => "Yes",
            "B" => "A",
            "C" => "B",
            "D" => "C"
        ]
    ],
    [
        'question' => 'Do you love me??????????',
        'answer' => [
            "A" => "Yes",
            "B" => "A",
            "C" => "B",
            "D" => "C"
        ]
    ],
    [
        'question' => 'Do you love me???????????',
        'answer' => [
            "A" => "Yes",
            "B" => "A",
            "C" => "B",
            "D" => "C"
        ]
    ]
    );
    session_start();
    if (isset($_POST["submit"])) {
        for ($i = 0; $i < 10; $i++) {
            setcookie('Q' . $i,$_POST['Q' . $i],time() + (86400 * 30), "/");
        }
        setcookie("list",json_encode($list_Q),time()+3600);
        header('Location: ' . 'result.php');
    }
    
?>
<html>
    <head>
        <title>Document</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    </head>
<style type="text/css">

    body{
        background-image: url("/day09/background/background.jpg");
    }

    .questions{
        display: inline;
        height: 500px;
        background-color: #FFFF;
    }
    .btn{
        display: inline-block;
        background: #41D2D5;
        padding: 10px 40px 10px 40px;
        border-radius: 10px/20px;
        border: 1px solid #000;
    }
    .next{
        float: right;
    }
</style>
<?php
?>
<body> 
    <form method="post">
        <div class="questions" id = "questions">
            <div id = "pageone">
            <?php
            for ($i = 0; $i < 5; $i++){
                echo "<h4> Câu ". ($i + 1). ": ". $list_Q[$i]["question"] . "</h2>";
                foreach ($list_Q[$i]['answer'] as $key => $value) {?>
                    <input type='radio' name = <?php echo "Q" . $i ?> id = <?php echo "Q" . $i ?> value=<?php echo $key?>>
                    <label for=<?php echo "Q" . $i ?>><?= $key . ". " . $value?></label>
                    <br>
                    <?php
                }
            }?>
            </div>
            <div id = "pagetwo">
            <?php
            for ($i = 5; $i < 10; $i++){
                echo "<h4> Câu ". ($i + 1). ": ". $list_Q[$i]["question"] . "</h2>";
                foreach ($list_Q[$i]['answer'] as $key => $value) {?>
                    <input type='radio' name = <?php echo "Q" . $i ?> id = <?php echo "Q" . $i ?> value=<?php echo $key?>>
                    <label for=<?php echo "Q" . $i ?>><?= $key . ". " . $value?></label>
                    <br>
                    <?php
                }
            }?>
            </div>
        </div>
        <label id = "next" class="btn next">Nextview</label>
        <label id = "prev" class="btn next">Preview</label>
        <button name = "submit" type = "submit" class = "btn next">Submit</button>
    </form>
</body>
<?php


?>
<script>
    $('#prev').hide();
    $('#pagetwo').hide();
    $('#next').click(function() {
        $('#prev').show();
        $('#next').hide();
        $('#pagetwo').show();
        $('#pageone').hide();
    });
    $('#prev').click(function() {
        $('#next').show();
        $('#prev').hide();
        $('#pageone').show();
        $('#pagetwo').hide();
    });
</script>
</html>